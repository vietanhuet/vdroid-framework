package vn.vdroid.configure;

public class AppConfigure {
	public static String DEFAULT_FONT = "museo";
	public static boolean TOAST_ENABLE = true;
	public static boolean LOG_ENABLE = true;
	public static String TAG = "vDroid";
}
