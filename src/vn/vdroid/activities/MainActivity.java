package vn.vdroid.activities;

import vn.vdroid.R;
import vn.vdroid.base.BaseActivity;
import vn.vdroid.fragments.SplashFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends BaseActivity {
	private static final long serialVersionUID = -7668273703026058116L;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					FragmentManager mFragmentManager = getSupportFragmentManager();
					mFragmentManager.beginTransaction().add(R.id.main_canvas, SplashFragment.getInstance()).commit();
					Thread.sleep(3000);
					//mFragmentManager.beginTransaction().replace(R.id.main_canvas, CanvasFragment.getInstance(RobotActivity.this)).commit();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}).start();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
}
