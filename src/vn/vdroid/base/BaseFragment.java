package vn.vdroid.base;

import java.io.Serializable;

import vn.vdroid.dialogs.TwoChosenDialog;
import vn.vdroid.listeners.TwoChosenDialogListener;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;

public abstract class BaseFragment extends Fragment implements OnClickListener, OnTouchListener, Serializable {

	private static final long serialVersionUID = 7080889824192321168L;
	protected FragmentManager mFragmentManager;
	protected BaseActivity mContext;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mFragmentManager = getFragmentManager();
		mContext = (BaseActivity) getActivity();
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onStop() {
		super.onStop();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
	
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			showToast("down");
			break;
		case MotionEvent.ACTION_MOVE:
			showToast("move");
			break;
		case MotionEvent.ACTION_UP:
			showToast("up");
			break;
		default:
			break;
		}
		
		return false;
	}

	@Override
	public void onClick(View view) {
		
	}
	

//	public void switchFragment(Fragment fragment) {
//		if (getActivity() == null) return;
//		((BaseActivity) getActivity()).switchContent(R.id.content_frame, fragment);
//	}
//	public void switchFragmentWithAnimation(Fragment fragment, int arg0, int arg1) {
//		if (getActivity() == null) return;
//		((BaseActivity) getActivity()).switchContentWithAnimation(R.id.content_frame, fragment, arg0, arg1);
//	}
//	public void switchFragmentWithAnimation(Fragment fragment, int arg0, int arg1, int arg2, int arg3) {
//		if (getActivity() == null) return;
//		((BaseActivity) getActivity()).switchContentWithAnimation(R.id.content_frame, fragment, arg0, arg1, arg2, arg3);
//	}
//	public void switchFragment(Fragment fragment, String tag) {
//		if (getActivity() == null) return;
//		((BaseActivity) getActivity()).switchContent(R.id.content_frame, fragment, tag);
//	}
//	public void switchFragmentWithAnimation(Fragment fragment, int arg0, int arg1, String tag) {
//		if (getActivity() == null) return;
//		((BaseActivity) getActivity()).switchContentWithAnimation(R.id.content_frame, fragment, arg0, arg1, tag);
//	}
//	public void switchFragmentWithAnimation(Fragment fragment, int arg0, int arg1, int arg2, int arg3, String tag) {
//		if (getActivity() == null) return;
//		((BaseActivity) getActivity()).switchContentWithAnimation(R.id.content_frame, fragment, arg0, arg1, arg2, arg3, tag);
//	}
	
	public void finishFragment() {
		try {
			mFragmentManager.popBackStack();
		} catch (Exception e) {
		//	Log.e(e.getMessage());
		}
	}
	
	protected abstract void initModels();
	protected abstract void initViews(View view);
	protected abstract void initAnimations();

	protected void removePreviousDialog(String tag) {
		FragmentTransaction ft = mFragmentManager.beginTransaction();
		Fragment prev = mFragmentManager.findFragmentByTag(tag);
		if (prev != null) ft.remove(prev);
		ft.commit();
	}

	public void showYesNoDialog(Context context, String content, String leftButton, String rightButton, TwoChosenDialogListener listener) {
		removePreviousDialog("yes_no_dialog");
		TwoChosenDialog mYesNoDialog = TwoChosenDialog.newInstance(mContext, content, leftButton, rightButton, listener);
		mYesNoDialog.show(mFragmentManager, "alert_dialog");
	}
	
//	public void closeProgressDialog() {
//		mProgressDialog.dismissDialog();
//		mProgressDialog.dismiss();
//	}
	
	public void logE(final String message) {
		mContext.logE(message);
	}
	public void logD(final String message) {
		mContext.logD(message);
	}
	public void showToast(String message) {
		mContext.showToast(message);
	}
	public void showProgressDialog(String message) {
		mContext.showProgressDialog(message);
	}
}
