package vn.vdroid.base;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import vn.vdroid.configure.AppConfigure;
import vn.vdroid.dialogs.ProgressDialog;
import vn.vdroid.dialogs.TwoChosenDialog;
import vn.vdroid.listeners.ProgressDialogListenter;
import vn.vdroid.listeners.TwoChosenDialogListener;
import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Toast;

public abstract class BaseActivity extends FragmentActivity implements OnClickListener, Serializable, OnTouchListener {

	private static final long serialVersionUID = -6132406776837910717L;

	public void switchContent(int contentId, Fragment fragment, String tag) {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(contentId, fragment);
		transaction.addToBackStack(tag);
		transaction.commitAllowingStateLoss();
	}
	public void switchContentWithAnimation(int contentId, Fragment fragment, int arg0, int arg1, String tag) {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.setCustomAnimations(arg0, arg1);
		transaction.replace(contentId, fragment);
		transaction.addToBackStack(tag);
		transaction.commitAllowingStateLoss();
	}
	public void switchContentWithAnimation(int contentId, Fragment fragment, int arg0, int arg1, int arg2, int arg3, String tag) {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.setCustomAnimations(arg0, arg1, arg2, arg3);
		transaction.replace(contentId, fragment);
		transaction.addToBackStack(tag);
		transaction.commitAllowingStateLoss();
	}
	public void switchContent(int contentId, Fragment fragment) {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(contentId, fragment);
		transaction.commitAllowingStateLoss();
	}
	public void switchContentWithAnimation(int contentId, Fragment fragment, int arg0, int arg1) {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.setCustomAnimations(arg0, arg1);
		transaction.replace(contentId, fragment);
		transaction.commitAllowingStateLoss();
	}
	public void switchContentWithAnimation(int contentId, Fragment fragment, int arg0, int arg1, int arg2, int arg3) {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.setCustomAnimations(arg0, arg1, arg2, arg3);
		transaction.replace(contentId, fragment);
		transaction.commitAllowingStateLoss();
	}
	
	@SuppressLint("NewApi")
	protected FragmentManager mFragmentManager = getFragmentManager();
	
	@SuppressLint("NewApi")
	protected void removePreviousDialog(String tag) {
		android.app.FragmentTransaction ft = mFragmentManager.beginTransaction();
		android.app.Fragment prev = mFragmentManager.findFragmentByTag(tag);
		if (prev != null) ft.remove(prev);
		ft.commit();
	}
	public void showTwoChosenDialog(String title, String content, String leftButton, String rightButton, TwoChosenDialogListener listener) {
		removePreviousDialog("yes_no_dialog");
		TwoChosenDialog mYesNoDialog = null;
		mYesNoDialog = TwoChosenDialog.newInstance(BaseActivity.this, content, leftButton, rightButton, listener);
		mYesNoDialog.show(getSupportFragmentManager(), "OK");
	}
	public void showProgressDialog(String content) {
		removePreviousDialog("progress_dialog");
		ProgressDialog mProgressDialog  = ProgressDialog.newInstance(BaseActivity.this, content, new ProgressDialogListenter() {
			
		});
		mProgressDialog.show(getSupportFragmentManager(), "OK");
	}
	public void showToast(final String message) {
		if(AppConfigure.TOAST_ENABLE) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(BaseActivity.this, message, Toast.LENGTH_LONG).show();
				}
			});
		}
	}
	public void logE(final String message) {
		if(AppConfigure.LOG_ENABLE) {
			Log.e(AppConfigure.TAG, message);
		}
	}
	public void logD(final String message) {
		if(AppConfigure.LOG_ENABLE) {
			Log.d(AppConfigure.TAG, message);
		}
	}
	public void openOtherApp(String appPackage) {
		PackageManager manager = getPackageManager();
		try {
		    Intent i = manager.getLaunchIntentForPackage(appPackage);
		    if (i == null){
		    	throw new PackageManager.NameNotFoundException();
		    }
		    i.addCategory(Intent.CATEGORY_LAUNCHER);
		    startActivity(i);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
	}
	public Bitmap getBitmapFromAsset(Context context, String filePath) {
	    AssetManager assetManager = context.getAssets();
	    InputStream istr;
	    Bitmap bitmap = null;
	    try {
	        istr = assetManager.open(filePath);
	        bitmap = BitmapFactory.decodeStream(istr);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return bitmap;
	}
}
