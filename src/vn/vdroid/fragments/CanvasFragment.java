package vn.vdroid.fragments;

import vn.vdroid.R;
import vn.vdroid.base.BaseFragment;
import vn.vdroid.customize.MyGifView;
import vn.vdroid.listeners.TwoChosenDialogListener;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CanvasFragment extends BaseFragment {

	private static final long serialVersionUID = 9158868130895684042L;
	private static CanvasFragment mInstance;
	
	public static CanvasFragment getInstance() {
		if (mInstance == null) {
			mInstance = new CanvasFragment();
		}
		return mInstance;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_canvas, container, false);
		
		final MyGifView gif = (MyGifView) view.findViewById(R.id.gif1);
		gif.setMovieResource(R.drawable.loading_setting);
		
		showYesNoDialog(getActivity(),"All data will be saved, continue ?", "OK", "Cancel", new TwoChosenDialogListener() {
			@Override
			public void onRightClick(Dialog dialog) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onLeftClick(Dialog dialog) {
				// TODO Auto-generated method stub
			}
		});		
	//	showProgressDialog(getActivity(), "Connecting to server ...");
		return view;
	}
	
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initModels() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initViews(View view) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initAnimations() {
		// TODO Auto-generated method stub
		
	}

}
