package vn.vdroid.fragments;

import vn.vdroid.R;
import vn.vdroid.base.BaseFragment;
import vn.vdroid.customize.MyGifView;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class SplashFragment extends BaseFragment {

	private static final long serialVersionUID = -3335182509181200753L;
	
	private static SplashFragment mInstance;
	public static SplashFragment getInstance() {
		if (mInstance == null) {
			mInstance = new SplashFragment();
		}
		return mInstance;
	}
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_splash, container, false);
		initViews(view);
		initModels();
		initAnimations();
		final MyGifView gif = (MyGifView) view.findViewById(R.id.splash_logo);
		gif.setMovieResource(R.drawable.loading_metro_white);
		return view;
	}
	
	public void onGifClick(View v) {
		MyGifView gif = (MyGifView) v;
		gif.setPaused(!gif.isPaused());
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	

	@Override
	protected void initModels() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initViews(View view) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initAnimations() {
		// TODO Auto-generated method stub
		
	}
}
